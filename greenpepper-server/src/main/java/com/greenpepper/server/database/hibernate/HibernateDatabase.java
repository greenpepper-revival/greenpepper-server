package com.greenpepper.server.database.hibernate;

import com.greenpepper.server.domain.*;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;

import java.util.EnumSet;
import java.util.Properties;

/**
 * <p>HibernateDatabase class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public class HibernateDatabase
{
    private final Configuration cfg;

    private final MetadataSources metadataSources;

    private final Metadata metadata;

    /**
     * <p>Constructor for HibernateDatabase.</p>
     *
     * @param properties a {@link java.util.Properties} object.
     * @throws org.hibernate.HibernateException if any.
     */
    public HibernateDatabase(Properties properties) throws HibernateException
    {
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();
        this.metadataSources = new MetadataSources( serviceRegistry );
        cfg = new Configuration(metadataSources);
        cfg.setProperties(properties); 
        setAnnotadedClasses();
        metadata = metadataSources.buildMetadata();
    }
    
    /**
     * <p>createDatabase.</p>
     *
     * @throws org.hibernate.HibernateException if any.
     */
    public void createDatabase() throws HibernateException
    {
        new SchemaExport().create(EnumSet.of(TargetType.DATABASE), metadata);
    }

    /**
     * <p>dropDatabase.</p>
     *
     * @throws org.hibernate.HibernateException if any.
     */
    public void dropDatabase() throws HibernateException
    {
        new SchemaExport().drop(EnumSet.of(TargetType.DATABASE), metadata);
    }
    
    /**
     * <p>getConfiguration.</p>
     *
     * @return a {@link org.hibernate.cfg.Configuration} object.
     */
    public Configuration getConfiguration()
    {
        return cfg;
    }    

    /**
     * <p>getSessionFactory.</p>
     *
     * @return a {@link org.hibernate.SessionFactory} object.
     * @throws org.hibernate.HibernateException if any.
     */
    public SessionFactory getSessionFactory() throws HibernateException
    {
        return cfg.buildSessionFactory();
    }

    public Metadata getMetadata() {
        return metadata;
    }

    private void setAnnotadedClasses()
    {
        cfg.addAnnotatedClass(SystemInfo.class)
        .addAnnotatedClass(Project.class)
        .addAnnotatedClass(Runner.class)
        .addAnnotatedClass(EnvironmentType.class)
        .addAnnotatedClass(Repository.class)
        .addAnnotatedClass(RepositoryType.class)
        .addAnnotatedClass(RepositoryTypeClass.class)
        .addAnnotatedClass(SystemUnderTest.class)
        .addAnnotatedClass(Requirement.class)
        .addAnnotatedClass(Specification.class)
        .addAnnotatedClass(Reference.class)
        .addAnnotatedClass(Execution.class);
    }

}
