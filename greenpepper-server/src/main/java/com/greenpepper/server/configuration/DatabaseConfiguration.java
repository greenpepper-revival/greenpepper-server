package com.greenpepper.server.configuration;

import org.apache.commons.text.StringSubstitutor;

import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.defaultString;

public abstract class DatabaseConfiguration {

    private String host;
    private String port;
    private String database;
    private String username;
    private String password;

    public Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.connection.url", getJDBCUrl());
        properties.put("hibernate.connection.driver_class", getDriverClass());
        properties.put("hibernate.connection.username", getUsername());
        properties.put("hibernate.connection.password", getPassword());
        properties.put("hibernate.dialect", getHibernateDialect());
        properties.put("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.show_sql", "false");
        properties.put("hibernate.setup", "true");
        return properties;
    }

    protected abstract String getJDBCUrlTemplate();
    protected abstract String getHibernateDialect();
    public abstract String getDriverClass();


    public String getJDBCUrl() {
        Properties values = new Properties();
        values.setProperty("host", defaultString(host));
        values.setProperty("port", defaultString(port));
        values.setProperty("database", defaultString(database));
        return StringSubstitutor.replace(getJDBCUrlTemplate(), values);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public static class PostgresqlConfiguration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:postgresql://${host}:${port}/${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.PostgreSQL92Dialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "org.postgresql.Driver";
        }
    }

    public static class MysqlConfiguration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:mysql://${host}:${port}/${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.MySQL5Dialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "com.mysql.cj.jdbc.Driver";
        }
    }

    public static class H2Configuration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:h2:file:${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.H2Dialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "org.h2.Driver";
        }
    }

    public static class H2MemConfiguration extends H2Configuration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:h2:mem:${database}";
        }
    }

    public static class H2ServerConfiguration extends H2Configuration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:h2:tcp://${host}:${port}/${database}";
        }
    }

    public static class OracleConfiguration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:oracle:thin:@${host}:${port}:${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.Oracle9iDialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "oracle.jdbc.OracleDriver";
        }
    }


    public static  class SQLServerConfiguration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:sqlserver://${host}:${port};databaseName=${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.SQLServerDialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "oracle.jdbc.OracleDriver";
        }
    }

    public static class HSQLConfiguration extends DatabaseConfiguration {

        @Override
        protected String getJDBCUrlTemplate() {
            return "jdbc:hsqldb:file:${database}";
        }

        @Override
        protected String getHibernateDialect() {
            return org.hibernate.dialect.HSQLDialect.class.getName();
        }

        @Override
        public String getDriverClass() {
            return "org.hsqldb.jdbc.JDBCDriver";
        }
    }
}
