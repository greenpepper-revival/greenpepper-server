package com.greenpepper.server;

import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.greenpepper.server.configuration.ServerConfiguration;
import com.greenpepper.server.database.hibernate.BootstrapData;
import com.greenpepper.server.database.hibernate.HibernateSessionService;
import com.greenpepper.util.URIUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>GPServletContextListener class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public class GPServletContextListener implements ServletContextListener
{
    private static final Logger logger = LoggerFactory.getLogger(GPServletContextListener.class);

    private static String GREENPEPPER_CONFIG = "greenpepper-server.cfg.xml";
    
    /** {@inheritDoc} */
    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        logger.info("******* Mounting up GreenPepper-Server");
        ServletContext ctx = servletContextEvent.getServletContext();
        try
        {
            URL url = GPServletContextListener.class.getClassLoader().getResource(GREENPEPPER_CONFIG);
            Properties sProperties = ServerConfiguration.load(url).getProperties();
            injectAdditionalProperties(ctx, sProperties);
            
            HibernateSessionService service = new HibernateSessionService(sProperties);
            ctx.setAttribute(ServletContextKeys.SESSION_SERVICE, service);
            
            logger.info("Boostrapping datas");
            new BootstrapData(service, sProperties).execute();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }      
    }

    /** {@inheritDoc} */
    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {
        ServletContext ctx = servletContextEvent.getServletContext();
        HibernateSessionService service = (HibernateSessionService)ctx.getAttribute(ServletContextKeys.SESSION_SERVICE);
        if(service != null) service.close();
    }
    
    private void injectAdditionalProperties(ServletContext ctx, Properties sProperties)
    {
        String dialect = ctx.getInitParameter("hibernate.dialect");        
        if(dialect != null) sProperties.setProperty("hibernate.dialect", dialect);
        if(ctx.getRealPath("/") != null) 
        {
            sProperties.setProperty("baseUrl", URIUtil.decoded(ctx.getRealPath("/")));
        }
    }
}
