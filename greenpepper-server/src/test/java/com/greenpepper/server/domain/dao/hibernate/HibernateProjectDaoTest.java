package com.greenpepper.server.domain.dao.hibernate;

import java.util.List;

import org.hibernate.Transaction;

import com.greenpepper.server.GreenPepperServerErrorKey;
import com.greenpepper.server.GreenPepperServerException;
import com.greenpepper.server.database.hibernate.hsqldb.AbstractDBUnitHibernateMemoryTest;
import com.greenpepper.server.domain.Project;
import com.greenpepper.server.domain.dao.ProjectDao;

public class HibernateProjectDaoTest extends AbstractDBUnitHibernateMemoryTest
{
    private static final String DATAS = "/dbunit/datas/HibernateProjectDaoTest.xml";
    private ProjectDao projectDao;

    protected void setUp() throws Exception
    {
        super.setUp();
        insertIntoDatabase(DATAS);        
        projectDao = new HibernateProjectDao(this);
	}

    public void testThatAProjectCanBeSelectedbyName() throws GreenPepperServerException
    {
        Project projectByName = projectDao.getByName("PROJECT-1");
        assertNotNull(projectByName);
    }

    public void testThatSelectByNameThrowsAnExceptionIfTheProjectIsNotPresisted()
    {
        assertNull(projectDao.getByName("PROJECT-NOT-FOUND"));
    }
    
    public void testThatAProjectCanBeCreated()
			throws GreenPepperServerException {
        
        Project projectCreated = projectDao.create("PROJECT-CREATED");
        
        
        assertNotNull(getById(Project.class, projectCreated.getId()));
        assertEquals("PROJECT-CREATED", projectCreated.getName());
    }
    
    public void testThatTheUnicityOfTheProjectNameOnCreation()
    {
        try
        {
            projectDao.create("PROJECT-1");
            fail();
        }
		catch (GreenPepperServerException ex) {
			assertEquals(GreenPepperServerErrorKey.PROJECT_ALREADY_EXISTS, ex.getId());
		}
	}
    
    public void testThatTheRepositoryAreWellAssociated() throws GreenPepperServerException
    {
        Project project = projectDao.getByName("PROJECT-1");
        assertEquals(1, project.getRepositories().size());
    }
    
    public void testThatWeCanRetrieveAllProjects()
    {
        List<Project> projects = projectDao.getAll();
        assertEquals(6, projects.size());
    }

	public void testThatWeCanRemoveAProjectThatIsNotAssociatedToEntities() throws GreenPepperServerException
	{
		int numberOfProjects = projectDao.getAll().size();
		projectDao.remove("PROJECT-TO-REMOVE-NOASSOCIATIONS");
		assertEquals(numberOfProjects - 1, projectDao.getAll().size());
	}

	public void testThatWeCannotRemoveAProjectAssociatedToARepository() throws GreenPepperServerException
	{
		Project projectToRemove = projectDao.getByName("PROJECT-TO-REMOVE-5");
		assertEquals(1, projectToRemove.getRepositories().size());

		try
		{
			projectDao.remove(projectToRemove.getName());
			fail();
		}
		catch (GreenPepperServerException ex)
		{
			assertEquals(GreenPepperServerErrorKey.PROJECT_REPOSITORY_ASSOCIATED, ex.getId());
		}
	}

	public void testThatWeCannotRemoveAProjectAssociatedToSUTS() throws GreenPepperServerException
	{
		Project projectToRemove = projectDao.getByName("PROJECT-TO-REMOVE-6");
		assertEquals(1, projectToRemove.getSystemUnderTests().size());

		try
		{
			projectDao.remove(projectToRemove.getName());
			fail();
		}
		catch (GreenPepperServerException ex)
		{
			assertEquals(GreenPepperServerErrorKey.PROJECT_SUTS_ASSOCIATED, ex.getId());
		}
	}

	public void testWeCantUpdateANotFoundProject()
	{
		try
		{
			
			projectDao.update("PROJECT-NOT-FOUND", Project.newInstance("TO-SOME-PROJECT"));
			
			fail();
		}
		catch (GreenPepperServerException e)
		{
			assertEquals(GreenPepperServerErrorKey.PROJECT_NOT_FOUND, e.getId());
		}
	}

	public void testWeCantUpdateAProjectToAnAlreadyUsedName()
	{
		try
		{
			
			projectDao.update("PROJECT-1", Project.newInstance("PROJECT-2"));
			
			fail();
		}
		catch (GreenPepperServerException e)
		{
			assertEquals(GreenPepperServerErrorKey.PROJECT_ALREADY_EXISTS, e.getId());
		}
	}

	public void testWeCanUpdateAProject()
			throws GreenPepperServerException
	{

		
		Project projectUpdated = projectDao.update("PROJECT-1", Project.newInstance("PROJECT-1-UPDATED"));
		

		assertNotNull(getById(Project.class, projectUpdated.getId()));
		assertEquals("PROJECT-1-UPDATED", projectUpdated.getName());
	}
}