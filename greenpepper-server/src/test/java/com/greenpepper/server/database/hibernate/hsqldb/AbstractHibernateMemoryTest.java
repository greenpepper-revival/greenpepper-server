package com.greenpepper.server.database.hibernate.hsqldb;


import org.hibernate.resource.transaction.spi.TransactionStatus;

public abstract class AbstractHibernateMemoryTest extends HibernateMemoryTestCase
{
    private String dataFile;

    protected void setUp() throws Exception
    {
        super.setUp();
        startSession();
    }

    protected void tearDown() throws Exception
    {
        rollbackIfNecessary();
        closeSession();
        super.tearDown();
    }

    private void rollbackIfNecessary()
    {
        if (transaction == null) return;
        if (!transaction.isActive()) return;
        if (transaction.getStatus()
                .isNotOneOf(TransactionStatus.COMMITTED, TransactionStatus.COMMITTING,
                        TransactionStatus.ROLLING_BACK,
                        TransactionStatus.ROLLED_BACK)) transaction.rollback();
    }

    public String getDataFile()
    {
        return dataFile;
    }

    public void setDataFile(String dataFile)
    {
        this.dataFile = dataFile;
    }
}
