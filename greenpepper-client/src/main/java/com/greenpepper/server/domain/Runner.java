package com.greenpepper.server.domain;

import com.greenpepper.report.XmlReport;
import com.greenpepper.server.GreenPepperServerException;
import com.greenpepper.server.rpc.xmlrpc.XmlRpcDataMarshaller;
import com.greenpepper.server.rpc.xmlrpc.client.XmlRpcClientExecutor;
import com.greenpepper.server.rpc.xmlrpc.client.XmlRpcClientExecutorFactory;
import com.greenpepper.util.*;
import com.greenpepper.util.cmdline.CommandLineBuilder;
import com.greenpepper.util.cmdline.CommandLineExecutor;
import org.hibernate.annotations.SortComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.greenpepper.server.rpc.xmlrpc.XmlRpcDataMarshaller.*;
import static com.greenpepper.util.IOUtils.uniquePath;
import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Runner Class.
 * Definition of a Runner.
 * <p/>
 * Copyright (c) 2006-2007 Pyxis technologies inc. All Rights Reserved.
 *
 * @author JCHUET
 * @version $Id: $Id
 */

@Entity
@Table(name="RUNNER")
@SuppressWarnings("serial")
public class Runner extends AbstractVersionedEntity implements Comparable<Runner>
{

    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);
	private static final String AGENT_HANDLER = "greenpepper-agent1";
    private String name;
    private String cmdLineTemplate;
    private String mainClass;
    private EnvironmentType envType;
    
    private String serverName;
    private String serverPort;
    private Boolean secured;
    
    private SortedSet<String> classpaths = new TreeSet<String>();
    
    /**
     * <p>newInstance.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link com.greenpepper.server.domain.Runner} object.
     */
    public static Runner newInstance(String name)
    {
        Runner runner = new Runner();
        runner.setName(name);
        
        return runner;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    @Column(name = "NAME", unique = true, nullable = false)
    public String getName()
    {
        return name;
    }

    /**
     * <p>getEnvironmentType.</p>
     *
     * @return a {@link com.greenpepper.server.domain.EnvironmentType} object.
     */
    @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="ENVIRONMENT_TYPE_ID")
    public EnvironmentType getEnvironmentType()
    {
        return envType;
    }

    /**
     * <p>Getter for the field <code>serverName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    @Column(name = "SERVER_NAME")
    public String getServerName()
    {
        return serverName;
    }
    
    /**
     * <p>Getter for the field <code>serverPort</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    @Column(name = "SERVER_PORT", length=8)
    public String getServerPort()
    {
        return serverPort;
    }

    /**
     * <p>isSecured.</p>
     *
     * @return a boolean.
     */

    @Column(name = "SECURED")
    public boolean isSecured()
    {
    	return secured != null && secured;
    }
    
    /**
     * <p>Getter for the field <code>cmdLineTemplate</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    @Column(name = "CMD_LINE_TEMPLATE", length=510)
    public String getCmdLineTemplate()
    {
        return cmdLineTemplate;
    }

    /**
     * <p>Getter for the field <code>mainClass</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */

    @Column(name = "MAIN_CLASS")
    public String getMainClass()
    {
        return mainClass;
    }

    /**
     * <p>Getter for the field <code>classpaths</code>.</p>
     *
     * @return a {@link java.util.SortedSet} object.
     */
    @ElementCollection
	@JoinTable( name="RUNNER_CLASSPATHS", joinColumns={@JoinColumn(name="RUNNER_ID")} )
	@Column(name = "elt")
	@SortComparator(ClasspathComparator.class)
	public SortedSet<String> getClasspaths()
    {
        return classpaths;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * <p>setEnvironmentType.</p>
     *
     * @param envType a {@link com.greenpepper.server.domain.EnvironmentType} object.
     */
    public void setEnvironmentType(EnvironmentType envType)
    {
        this.envType = envType;
    }

    /**
     * <p>Setter for the field <code>serverName</code>.</p>
     *
     * @param serverName a {@link java.lang.String} object.
     */
    public void setServerName(String serverName)
    {
        this.serverName = StringUtil.toNullIfEmpty(serverName);
    }

    /**
     * <p>Setter for the field <code>serverPort</code>.</p>
     *
     * @param serverPort a {@link java.lang.String} object.
     */
    public void setServerPort(String serverPort)
    {
        this.serverPort = StringUtil.toNullIfEmpty(serverPort);
    }
    
    /**
     * <p>Setter for the field <code>secured</code>.</p>
     *
     * @param secured a {@link java.lang.Boolean} object.
     */
    public void setSecured(Boolean secured)
    {
        this.secured = secured != null && secured;
    }

    /**
     * <p>Setter for the field <code>cmdLineTemplate</code>.</p>
     *
     * @param cmdLineTemplate a {@link java.lang.String} object.
     */
    public void setCmdLineTemplate(String cmdLineTemplate)
    {
        this.cmdLineTemplate = StringUtil.toNullIfEmpty(cmdLineTemplate);
    }
    
    /**
     * <p>Setter for the field <code>mainClass</code>.</p>
     *
     * @param mainClass a {@link java.lang.String} object.
     */
    public void setMainClass(String mainClass)
    {
        this.mainClass = StringUtil.toNullIfEmpty(mainClass);
    }

    /**
     * <p>Setter for the field <code>classpaths</code>.</p>
     *
     * @param classpaths a {@link java.util.SortedSet} object.
     */
    public void setClasspaths(SortedSet<String> classpaths)
    {
        this.classpaths = classpaths;
    }

    /**
     * <p>execute.</p>
     *
     * @param specification a {@link com.greenpepper.server.domain.Specification} object.
     * @param systemUnderTest a {@link com.greenpepper.server.domain.SystemUnderTest} object.
     * @param implementedVersion a boolean.
     * @param sections a {@link java.lang.String} object.
     * @param locale a {@link java.lang.String} object.
     * @return a {@link com.greenpepper.server.domain.Execution} object.
     */
    public Execution execute(Specification specification, SystemUnderTest systemUnderTest, boolean implementedVersion, String sections, String locale)
    {
    	if(isRemote())
    	{
    		return executeRemotely(specification, systemUnderTest, implementedVersion, sections, locale);
    	}
    	else
    	{
    		return executeLocally(specification, systemUnderTest, implementedVersion, sections, locale);
    	}
    }
    
    @SuppressWarnings("unchecked")
    protected Execution executeRemotely(Specification specification, SystemUnderTest systemUnderTest, boolean implementedVersion, String sections, String locale)
    {
        LOG.debug("Execute Remotely {} on agentURL {}", specification.getName(), agentUrl());

        try
        {
        	sections = defaultString(sections);
        	locale = defaultString(locale);

	        XmlRpcClientExecutor xmlrpc = XmlRpcClientExecutorFactory.newExecutor(agentUrl());

	        Vector params = CollectionUtil.toVector(marshallize(), systemUnderTest.marshallize(), specification.marshallize(), implementedVersion, sections, locale);

	        Vector<Object> execParams = (Vector<Object>)xmlrpc.execute(AGENT_HANDLER+".execute", params);
	        
			Execution execution = XmlRpcDataMarshaller.toExecution(execParams);
	        execution.setSystemUnderTest(systemUnderTest);
	        execution.setSpecification(specification);
			execution.setRemotelyExecuted();
			return execution;
        }
        catch (Exception e)
        {
            return Execution.error(specification, systemUnderTest, sections, ExceptionUtils.stackTrace(e, "<br>", 15));
        }
    }
    
    private Execution executeLocally(Specification specification, SystemUnderTest systemUnderTest, boolean implementedVersion, String sections, String locale) {
        try {
            String outpuPath = uniquePath("GreenPepperTest", ".tst");
            return executeLocally(specification, systemUnderTest, implementedVersion, sections, locale, outpuPath);
        } catch (IOException e) {
            return Execution.error(specification, systemUnderTest, sections, ExceptionUtils.stackTrace(e, "<br>", 15));
        }

    }

    protected Execution executeLocally(Specification specification, SystemUnderTest systemUnderTest,
                                       boolean implementedVersion, String sections, String locale, String outpuPath) {
        return executeLocally(specification, systemUnderTest, implementedVersion, sections, locale, outpuPath, null, null, false);
    }

    protected Execution executeLocally(Specification specification, SystemUnderTest systemUnderTest,
                                       boolean implementedVersion, String sections, String locale, String outpuPath,
                                       String stdOutFile, String stdErrFile, boolean redirectToLogFile) {
        File outputFile = null;
        CommandLineExecutor commandLineExecutor = null;
        try
        {
            outputFile = new File(outpuPath);

            String[] cmdLine = compileCmdLine(specification, systemUnderTest, outpuPath, implementedVersion, sections, locale);
            commandLineExecutor = new CommandLineExecutor(cmdLine);
            commandLineExecutor.setDontShowStdOut(redirectToLogFile);
            if (stdOutFile != null) {
                commandLineExecutor.setStdOutFile(stdOutFile);
            }
            if (stdErrFile != null) {
                commandLineExecutor.setStdErrFile(stdErrFile);
            }
            commandLineExecutor.executeAndWait();

            Execution execution = Execution.newInstance(specification, systemUnderTest, XmlReport.parse(outputFile));
            injectLogsInExecution(commandLineExecutor, execution);
            return execution;
        }
        catch (GreenPepperServerException e) {
            Execution execution = Execution.error(specification, systemUnderTest, sections, e.getId());
            if (commandLineExecutor != null) {
                injectLogsInExecution(commandLineExecutor, execution);
            }
            return execution;
        } catch (Exception e) {
            Execution execution = Execution.error(specification, systemUnderTest, sections, ExceptionUtils.stackTrace(e, "<br>", 15));
            if (commandLineExecutor != null) {
                injectLogsInExecution(commandLineExecutor, execution);
            }
            return execution;
        } finally {
            IOUtil.deleteFile(outputFile);
        }
    }

    private void injectLogsInExecution(CommandLineExecutor commandLineExecutor, Execution execution) {
        execution.setStdoutLogs(commandLineExecutor.getOutput());
        execution.setStderrLogs(commandLineExecutor.getError());
    }

    /**
	 * <p>marshallize.</p>
	 *
	 * @return a {@link java.util.Vector} object.
	 */
	public Vector<Object> marshallize()
    {
        Vector<Object> parameters = new Vector<Object>();
        parameters.add(RUNNER_NAME_IDX, name);
        parameters.add(RUNNER_CMDLINE_IDX, defaultString(cmdLineTemplate));
        parameters.add(RUNNER_ENVTYPE_IDX, envType != null ? envType.marshallize() : EnvironmentType.newInstance("").marshallize());
        parameters.add(RUNNER_SERVER_NAME_IDX, defaultString(serverName));
        parameters.add(RUNNER_SERVER_PORT_IDX, defaultString(serverPort));
        parameters.add(RUNNER_MAINCLASS_IDX, defaultString(mainClass));
        parameters.add(RUNNER_CLASSPATH_IDX, new Vector<String>(classpaths));
        parameters.add(RUNNER_SECURED_IDX, isSecured());
        return parameters;
    }

    /**
     * <p>agentUrl.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String agentUrl() 
	{
		return ( isSecured() ? "https://" : "http://" ) + serverName + ":" + serverPort;
	}
    
    /** {@inheritDoc} */
    public int compareTo(Runner o)
    {
        return this.getName().compareTo(o.getName());
    }

    /** {@inheritDoc} */
    public boolean equals(Object o)
    {
        if(!(o instanceof Runner))
        {
            return false;
        }

        Runner runnerCompared = (Runner)o;
		return getName().equals(runnerCompared.getName());
	}

    /**
     * <p>hashCode.</p>
     *
     * @return a int.
     */
    public int hashCode()
    {
        return getName().hashCode();
    }
    
    private String[] compileCmdLine(Specification spec, SystemUnderTest sut, String outpuPath, boolean implementedVersion, String sections, String locale) throws Exception
    {
        CommandLineBuilder cmdBuilder = new CommandLineBuilder(cmdLineTemplate);
        cmdBuilder.setDependencies(mergedDependencies(sut));
        cmdBuilder.setMainClass(mainClass);
        cmdBuilder.setInputPath(URIUtil.raw(spec.getName()) + (implementedVersion ? "" : "?implemented=false"));
        cmdBuilder.setOutpuPath(outpuPath);
        cmdBuilder.setRepository(spec.getRepository().asCmdLineOption(envType));
        if (cmdLineTemplate.contains("{fixtureFactoryArgs}")) {
            cmdBuilder.setFixtureFactory(sut.getFixtureFactory());
            cmdBuilder.setFixtureFactoryArgs(sut.getFixtureFactoryArgs());
        } else {
            cmdBuilder.setFixtureFactory(sut.fixtureFactoryCmdLineOption());
        }
        cmdBuilder.setDialectClass(spec.getDialectClass());
		cmdBuilder.setProjectDependencyDescriptor(sut.getProjectDependencyDescriptor());
		cmdBuilder.setSections(sections);
        cmdBuilder.setLocale(locale);
        
        return cmdBuilder.getCmdLine();
    }

    private Collection<String> mergedDependencies(SystemUnderTest systemUnderTest)
    {
        Collection<String> dependencies = new ArrayList<String>();
        dependencies.addAll(getClasspaths());
        dependencies.addAll(systemUnderTest.getFixtureClasspaths());
        dependencies.addAll(systemUnderTest.getSutClasspaths());
        return dependencies;
    }
    
    @Transient
    protected boolean isRemote()
    {
    	return !StringUtil.isEmpty(serverName) && !StringUtil.isEmpty(serverPort);
    }
}
