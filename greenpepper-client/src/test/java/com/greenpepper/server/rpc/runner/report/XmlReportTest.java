/**
 * Copyright (c) 2008 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */
package com.greenpepper.server.rpc.runner.report;

import java.io.StringWriter;
import java.io.Writer;

import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.text.StringContains.containsString;
import static org.junit.Assert.assertThat;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;

import com.greenpepper.server.domain.Execution;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

public class XmlReportTest
{

	private static final String simpleResultString =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<documents>\n" +
			"    <document>\n" +
			"        <statistics>\n" +
			"            <success>0</success>\n" +
			"            <failure>0</failure>\n" +
			"            <error>0</error>\n" +
			"            <ignored>0</ignored>\n" +
			"        </statistics>\n" +
			"        <results><![CDATA[<html>test</html>]]></results>\n" +
			"    </document>\n" +
			"</documents>";

	private final Mockery context = new Mockery()
	{
		{
			setImposteriser(ClassImposteriser.INSTANCE);
		}
	};

	private Writer writer;
	private XmlReport xmlReport;

	@Before
	public void setUp()
			throws Exception
	{
		writer = context.mock(Writer.class);
		xmlReport = XmlReport.newInstance("test");
	}

	@After
	public void tearDown()
			throws Exception
	{
		context.assertIsSatisfied();
	}

	@Test
	public void testThatNothingIsPrintedWhenNoExecution()
			throws Exception
	{
		xmlReport.printTo(writer);
	}

	@Test
	public void testThatExceptionIsPrintedCorrectly()
			throws Exception
	{
		xmlReport.renderException(new NullPointerException("testThatExceptionIsPrintedCorrectly"));

		Writer writer = new StringWriter();

		xmlReport.printTo(writer);

		assertThat(writer.toString(), containsString("<![CDATA[java.lang.NullPointerException: testThatExceptionIsPrintedCorrectly"));
	}

	@Test
	public void testThatResultIsPrintedCorrectly()
			throws Exception
	{
		Execution execution = new Execution();
		execution.setResults("<html>test</html>");

		xmlReport.generate(execution);

		Writer writer = new StringWriter();

		xmlReport.printTo(writer);

		String report = writer.toString();
		assertThat(report, containsString("<success>0</success>"));
		assertThat(report, containsString("<failure>0</failure>"));
		assertThat(report, containsString("<error>0</error>"));
		assertThat(report, containsString("<![CDATA[<html>test</html>]]>"));
	}
}